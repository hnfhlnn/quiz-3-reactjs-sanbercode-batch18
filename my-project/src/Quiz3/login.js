import React, {useState} from "react"
import { useAuth } from "./context/auth";
import { Redirect } from "react-router-dom";
import './style.css';

/*
export default class Login extends React.Component {
    render() {
        return (
            <div class="myApp">
                <section>
	                <div>
                        <form method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="name">Username: </label>
                                <input class="form-control" type="text" name="name" placeholder="username"/>
	                        </div>
                            <div class="form-group">
                                <label for="password">Password: </label>
                                <input class="form-control" type="text" name="password" placeholder="password"/>
	                        </div>
                            <button>Login</button>
                        </form>
                    </div>
                </section>
                <footer>
                    <h5>copyright &copy; 2020 by Sanbercode</h5>
                </footer>
            </div>
        );
    }
}*/

const UserData = [
    { username: 'sanbercode', password: 'sanber'},
    { username: 'abcde', password: '12345'},
    { username: 'haniifah', password: '123'},
    { username: 'a', password: '1'}
  ];
  

  function Login() {
    const [input, setInput] =  useState({username:"", passwrod:""})
    const [isError, setIsError] = useState(null)
    const [isValid, setIsValid] = useState(false)
    
    
     const handleChangeNama = (event)=>{
        var value= event.target.value
        setInput({...input, username: value})
      }

      const handleChangePassword = (event)=>{
        var value= event.target.value
        setInput({...input, password: value})
      }

      const submitForm = (event) =>{

        event.preventDefault()
    
        let indexU = UserData.findIndex(x => x.username === input.username)
        let indexP = UserData.findIndex(x => x.password === input.password)
    
        if (indexU !== -1) {
          if (indexP === indexU) {
            setIsValid(true)
          } else {
            setIsError('Password Salah' );
          }
        } else {
            setIsError('Username belum terdaftar' );
        }

      }

      if (isValid) {
        return <Redirect to="/" />;
      }
    
        return (
            <div class="myApp app1">
                <section>
	                <div>
                        <form onSubmit={submitForm} method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="name">Username: </label>
                                <input class="form-control" type="text" name="username" value={input.username} onChange={handleChangeNama}/>
	                        </div>
                            <div class="form-group">
                                <label for="password">Password: </label>
                                <input class="form-control" type="text" name="password" value={input.password} onChange={handleChangePassword}/>
	                        </div>
                            <button>Login</button>
                        </form>
                    </div>
                </section>
                <footer>
                    <h5>copyright &copy; 2020 by Sanbercode</h5>
                </footer>
            </div>
        );
}

export default Login;