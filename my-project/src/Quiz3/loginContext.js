import React, { useState, createContext } from "react";

export const UserDataContext = createContext();

export const ListMovieProvider = props => {
  const [userData, setUserData] = useState({
    userName: "",
    password: "",
    isError: "",
    isValid: false
  });

  return (
    <UserDataContext.Provider value={[userData, setUserData]}>
      {props.children}
    </UserDataContext.Provider>
  );
};
