import React, {useContext, useEffect, useState} from "react"
import axios from "axios"
import {ListMovieContext} from "./listMovieContext"

const SomeMovieList = () =>{

  const [listMovie, setListMovie] =  useContext(ListMovieContext)
  const [title, setTitle] =  useState("")

    useEffect( () => {
        if (listMovie.lists === null){
          axios.get(`http://backendexample.sanbercloud.com/api/movies`)
          .then(res => {
            let listMovie = res.data
            setListMovie({
                ...listMovie, 
                lists: res.data.map(el=>{ 
                  return {
                    id:el.id,
                    title:el.title,
                    description:el.description,
                    year:el.year,
                    duration:el.duration,
                    genre:el.genre,
                    rating:el.rating,
                    //review:el.review,
                    image_url:el.image_url
                }
            })
          })
        })
      }
    }, [setListMovie, listMovie])

  const handleEdit = (event) =>{
    let idMovie = parseInt(event.target.value)
    setListMovie({...listMovie, selectedId: idMovie, statusForm: "changeToEdit"})
  }

  const handleDelete = (event) => {
    let idMovie = parseInt(event.target.value)

    let newLists = listMovie.lists.filter(el => el.id !== idMovie)

    axios.delete(`http://backendexample.sanbercloud.com/api/movies/${idMovie}`)
    .then(res => {
      console.log(res)
    })
          
    setListMovie({...listMovie, lists: [...newLists]})
    
  }

  const handleChangeTitle = (event)=>{
    var value= event.target.value
    setTitle(value)

    axios.get(`http://backendexample.sanbercloud.com/api/movies`)
          .then(res => {
            let listMovie = res.data
            setListMovie({...listMovie, 
                lists: res.data.map(el=>{ 
                  return {
                    id:el.id,
                    title:el.title,
                    description:el.description,
                    year:el.year,
                    duration:el.duration,
                    genre:el.genre,
                    rating:el.rating,
                    image_url:el.image_url
                  }
                })
            })
          })

  }

  const Search = (event) => {
    if (title !=="") {
      let newLists = listMovie.lists.filter(el => (el.title).includes(title))
      setListMovie({...listMovie, lists: [...newLists]})
    }
  }


  return(
    <>
      <div className="cari">
        <input style={{display:"inline-block"}} type="text" name="title" value={title} onChange={handleChangeTitle}/>
        <button style={{display:"inline-block"}} onClick={Search}>search</button>
      </div>
      <h1>Daftar Film</h1>
      <table>
        <thead>
          <tr>
            <th>No</th>
            <th>Title</th>
            <th>Description</th>
            <th>Year</th>
            <th>Duration</th>
            <th>Genre</th>
            <th>Rating</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>

            {
              listMovie.lists !== null && listMovie.lists.map((item, index)=>{
                return(                    
                  <tr key={index}>
                    <td>{index+1}</td>
                    <td>{item.title}</td>
                    <td>{item.description}</td>
                    <td>{item.year}</td>
                    <td>{item.duration}</td>
                    <td>{item.genre}</td>
                    <td>{item.rating}</td>
                    <td>
                      <button onClick={handleEdit} value={item.id}>Edit</button>
                      &nbsp;
                      <button onClick={handleDelete} value={item.id}>Delete</button>
                    </td>
                  </tr>
                )
              })
            }
        </tbody>
      </table>      
    </>
  )
}

export default SomeMovieList
