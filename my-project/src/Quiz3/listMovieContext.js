import React, { useState, createContext } from "react";

export const ListMovieContext = createContext();

export const ListMovieProvider = props => {
  const [listMovie, setListMovie] = useState({
    lists: null,
    selectedId: 0,
    statusForm: "create",
    user: ""
  });

  return (
    <ListMovieContext.Provider value={[listMovie, setListMovie]}>
      {props.children}
    </ListMovieContext.Provider>
  );
};
