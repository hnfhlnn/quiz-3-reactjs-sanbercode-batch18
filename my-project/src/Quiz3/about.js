import React from 'react';
import './style.css';

export default class Home extends React.Component {
    render() {
        return (
            <div class="myApp app1">
                <section>
	                <div class="border">
                        <h1>Data Peserta Sanbercode Pelatihan Reactjs</h1>
                        <div id="biodata">
                            <div>
		                        <p class="data"><b>1. Nama: </b>Hanifah Lainun</p>
		                        <p class="data"><b>2. Email: </b>hanifahl</p>
		                        <p class="data"><b>3. Sistem Operasi yang Digunakan: </b>Windows</p>
		                        <p class="data"><b>4. Akun Gitlab: </b><a href="https://gitlab.com/hnfhlnn/">https://gitlab.com/hnfhlnn/</a></p>
		                        <p class="data"><b>5. Akun Telegram: </b>@haniifahL</p>
                            </div>
                        </div>
	                </div>
                </section>
                <footer>
                    <h5>copyright &copy; 2020 by Sanbercode</h5>
                </footer>
            </div>
        );
    }
}