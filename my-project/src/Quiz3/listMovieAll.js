import React from "react"
import {ListMovieProvider} from "./listMovieContext"
import ListMovie from "./listMovie"
import ListMovieForm from "./listMovieForm"
import "./style.css"

const Movie = () =>{
  return(
    <div className="myApp app2">
        <section >
            <ListMovieProvider>
                <ListMovie/>
                <ListMovieForm/>
            </ListMovieProvider>
        </section>
    </div>
  )
}

export default Movie
