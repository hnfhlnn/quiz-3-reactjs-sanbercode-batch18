import React from "react"
import { Link } from "react-router-dom";
import './style.css';
import logo from './aset/logo.png';

const Nav = () =>{
  return(
        <header>
            <img id="logo" src={logo} width="200px" alt="logo"/>
            <nav>
                 <ul>
                    <li><Link to="/">Home </Link> </li>
                    <li><Link to="/about">About </Link> </li>
                    <li><Link to="/login">Login </Link> </li>
                    <li><Link to="/listMovie">Movie List Editor</Link> </li>
                </ul>
            </nav>
        </header>
  )
}

export default Nav
