import React from "react";
import { Switch, Route, BrowserRouter as Router } from "react-router-dom";
import Home from './Home.js';
import About from './about.js';
import Login from './login.js';
import ListMovie from './listMovieAll.js';
import Nav from './nav.js';

export default function App() {
  return (
      <>
        <Router>
            <Nav/>
            <Switch>
              <Route exact path="/">
                <Home />
              </Route>

              <Route exact path="/about">
                <About />
              </Route>
              <Route exact path="/login">
                <Login />
              </Route>
              <Route exact path="/listMovie">
                <ListMovie />
              </Route>
            </Switch>
        </Router>    
      </>
  );
}
