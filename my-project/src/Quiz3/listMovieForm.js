import React, {useContext, useState, useEffect} from "react"
import axios from "axios"
import {ListMovieContext} from "./listMovieContext.js"

const ListMovieForm = () =>{
  const [listMovie, setListMovie] = useContext(ListMovieContext)
  const [input, setInput] = useState({title: "", description: "", year:0, duration:0, genre: "", rating:0, image_url:""})

  useEffect(()=>{
    if (listMovie.statusForm === "changeToEdit"){
      let dataMovie = listMovie.lists.find(x=> x.id === listMovie.selectedId)
      setInput({
          title: dataMovie.title,
          description: dataMovie.description,
          year: dataMovie.year,
          duration: dataMovie.duration,
          genre: dataMovie.genre,
          rating: dataMovie.rating,
          //review: dataMovie.review,
          image_url: dataMovie.image_url
        })
      setListMovie({...listMovie, statusForm: "edit"})
    }
  },[listMovie, setListMovie])

  const handleChange = (event) =>{
    let typeOfInput = event.target.name

    switch (typeOfInput){
      case "title":
      {
        setInput({...input, title: event.target.value});
        break
      }
      case "description":
      {
        setInput({...input, description: event.target.value});
        break
      }
      case "year":
      {
        setInput({...input, year: event.target.value});
          break
      }
      case "duration":
      {
        setInput({...input, duration: event.target.value});
        break
      }
      case "genre":
      {
        setInput({...input, genre: event.target.value});
        break
      }
      case "rating":
      {
        setInput({...input, rating: event.target.value});
          break
      }
      case "image_url":
      {
        setInput({...input, image_url: event.target.value});
          break
      }
    default:
      {break;}
    }
  }
  
  const handleSubmit = (event) =>{
    // menahan submit
    event.preventDefault()

    let title = input.title
    let description = input.description
    let year = input.year
    let duration = input.duration
    let genre = input.genre
    let rating = input.rating
    let image_url = input.image_url

    //console.log(listMovie.statusForm)

    if (listMovie.statusForm === "create" || listMovie.statusForm ===undefined){        
      axios.post(`http://backendexample.sanbercloud.com/api/movies`, {title,description,year,duration,genre,rating,image_url})
      .then(res => {
          setListMovie(
            {statusForm: "create", selectedId: 0,
            lists: [
              ...listMovie.lists, 
              {
                id: res.data.id, 
                title: input.title,
                description: input.description,
                year: input.year,
                duration: input.duration,
                genre: input.genre,
                rating: input.rating,
                image_url: input.image_url
              }]
            })
      })
    }else if(listMovie.statusForm === "edit"){
      axios.put(`http://backendexample.sanbercloud.com/api/movies/${listMovie.selectedId}`, {title,description,year,duration,genre,rating,image_url})
      .then(() => {
          let dataMovie = listMovie.lists.find(el=> el.id === listMovie.selectedId)
          dataMovie.title = input.title
          dataMovie.description = input.description
          dataMovie.year = input.year
          dataMovie.duration = input.duration
          dataMovie.genre = input.genre
          dataMovie.rating = input.rating
          dataMovie.image_url = input.image_url
          setListMovie({statusForm: "create", selectedId: 0, lists: [...listMovie.lists]})
      })
    }

    setInput({title: "", description: "", year:0, duration:0, genre: "", rating:0, image_url:""})

  }
  return(
    <>
      <h1>Movies form</h1>

      <div style={{width: "60%", margin: "0 auto", display: "block"}}>
        <div style={{border: "1px solid #aaa", padding: "20px"}}>
          <form onSubmit={handleSubmit}>
            <label style={{float: "left"}}>
              Title:
            </label>
            <input style={{float: "right"}} type="text" name="title" value={input.title} onChange={handleChange} size="32"/>
            <br/>
            <br/>
            <label style={{float: "left"}}>
              Description:
            </label>
            <textarea style={{float: "right"}} type="text" name="description" value={input.description} onChange={handleChange}/>
            <br/>
            <br/>
            <br/>
            <label style={{float: "left"}}>
              Year:
            </label>
            <input style={{float: "right"}} type="number" name="year" value={input.year} onChange={handleChange} width="32"/>
            <br/>
            <br/>
            <label style={{float: "left"}}>
              Duration:
            </label>
            <input style={{float: "right"}} type="number" name="duration" value={input.duration} onChange={handleChange} size="32"/>
            <br/>
            <br/>
            <label style={{float: "left"}}>
              Genre:
            </label>
            <input style={{float: "right"}} type="text" name="genre" value={input.genre} onChange={handleChange} size="32"/>
            <br/>
            <br/>
            <label style={{float: "left"}}>
              Rating:
            </label>
            <input style={{float: "right"}} type="number" name="rating" value={input.rating} onChange={handleChange} size="32"/>
            <br/>
            <br/>
            <label style={{float: "left"}}>
              Image Url:
            </label>
            <textarea style={{float: "right"}} type="text" name="image_url" value={input.image_url} onChange={handleChange}/>
            <br/>
            <br/>
            <br/>
            <div style={{width: "100%", paddingBottom: "20px"}}>
              <button style={{ float: "right"}}>submit</button>
            </div>
          </form>
        </div>
      </div>
    </>
  )
}

export default ListMovieForm
