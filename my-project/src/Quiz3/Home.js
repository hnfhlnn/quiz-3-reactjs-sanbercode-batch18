import React, {useState, useEffect} from "react"
import axios from "axios"
import './style.css';

const Home = () => {
    const [listMovie, setListMovie] =  useState(null)

    useEffect( () => {
        if (listMovie === null){
          axios.get(`http://backendexample.sanbercloud.com/api/movies`)
          .then(res => {
            let listMovie = res.data
            setListMovie(
              listMovie.map(el=> {
                return {
                    id:el.id,
                    title:el.title,
                    description:el.description,
                    year:el.year,
                    duration:el.duration,
                    genre:el.genre,
                    rating:el.rating,
                    //review:el.review,
                    image_url:el.image_url
                }
              })
            )      
          })
        }
      },[listMovie])

        return (
            <div className="myApp app2">
                
                <section >
                    <h1>Daftar Film Terbaik</h1>
                    <div id="article-list">
                        {listMovie !== null && (
                            listMovie.map((el,index) => {
                                return (
                                    <div className="daftar" key={index}>
                                        <h3>{el.title}</h3>
                                        <div className="kotak">
                                            <div className="gambar">
                                                <img src={el.image_url} alt="poster"/>
                                            </div>
                                            <div className="details">
                                                <div className="nama detail">Rating: {el.rating}</div>
                                                <div className="nama detail">Durasi: {el.duration}</div>
                                                <div className="nama detail">Genre: {el.genre}</div>
                                            </div>
                                        </div>
                                        <p>
                                            <b>Deskripsi:</b> {el.description}
                                        </p>
                                    </div>
                                )
                            })
                        )}
                    </div>
                </section>
                <footer>
                    <h5>copyright &copy; 2020 by Sanbercode</h5>
                </footer>
            </div>
        );
}

export default Home;